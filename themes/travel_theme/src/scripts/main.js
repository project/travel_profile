import formError from './modules/form-error'
import clientBlock from './modules/clients-block'

(($, Drupal) => {
  formError($, Drupal)
  clientBlock($, Drupal)
})(jQuery, Drupal)
