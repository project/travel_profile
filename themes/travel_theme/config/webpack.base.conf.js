const StyleLintPlugin = require('stylelint-webpack-plugin')

process.noDeprecation = true

module.exports = {
  module: {
    rules: [{
      enforce : 'pre',
      test    : /\.js$/,
      exclude : [/node_modules/],
      loader  : 'eslint-loader'
    },
    {
      test    : /\.js?$/,
      exclude : [/node_modules/],
      loader  : 'babel-loader'
    },
    {
      test    : /\.(jpe?g|png|gif|svg)$/i,
      loader  : 'file-loader',
      options : {
        name: '[name].[hash:base64:5].[ext]',
        outputPath : 'assets/images/'
      }
    },
    {
      test    : /\.(woff(2)?|eot|ttf|otf)$/,
      loader  : 'file-loader',
      options : {
        name       : '[name].[ext]',
        outputPath : 'assets/fonts/'
      }
    }]
  },
  plugins: [
    new StyleLintPlugin()
  ]
}
