export default function ($, Drupal) {
  const drupal = Drupal
  drupal.behaviors.mytheme = {
    attach(context) {
      if (context === document) {
        const errName = $('#webform-client-form-2', context).find('.webform-component--name').find('input.error')
        const errMail = $('#webform-client-form-2', context).find('.webform-component--email').find('input.error')
        const errPhone = $('#webform-client-form-2', context).find('.webform-component--phone').find('input.error')
        if (errName) {
          $('input.error').addClass('form-error')
        } else if (errMail) {
          $('input.error').addClass('form-error')
        } else if (errPhone) {
          $('input.error').addClass('form-error')
        }
      }
    }
  }
}
