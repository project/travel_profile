const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin')

const base = require('./webpack.base.conf')

const configDev = {
  mode: 'development',
  watch: true,
  devtool: 'source-map',
  output: {
    filename: 'js/[name].app.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '../'
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: [{
      test: /(\.scss|\.css)$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        {
          loader: 'css-loader',
          options: {
            minimize: false,
            sourceMap: true
          }
        },
        {
          loader: 'resolve-url-loader'
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ]
    }]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], { verbose: false }),

    new MiniCssExtractPlugin({
      filename: "css/[name].css",
      chunkFilename: "css/[id].css"
    }),

    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(false)
    })
  ]
}

module.exports = merge(base, configDev)
