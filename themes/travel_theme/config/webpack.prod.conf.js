const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ImageminPlugin = require('imagemin-webpack-plugin').default

const base = require('./webpack.base.conf')

const configProd = {
  mode: 'production',
  output: {
    filename: 'js/[name].[chunkhash].app.js',
    path: path.resolve(__dirname, '../dist')
  },
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: [{
      test: /(\.scss|\.css)$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        {
          loader: 'css-loader'
        },
        {
          loader: 'postcss-loader',
          options: {
            ident: 'postcss',
            plugins: () => [
              require('cssnano')({
                preset: 'default',
              }),
              require('autoprefixer')
            ]
          }
        },
        {
          loader: 'resolve-url-loader'
        },
        {
          loader: 'sass-loader'
        }
      ]
    }]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], { verbose: false }),

    new MiniCssExtractPlugin({
      filename: "css/[name].[hash].css",
      chunkFilename: "css/[id].[hash].css"
    }),

    new ImageminPlugin({
      pngquant: {
        quality: '65-80'
      }
    }),

    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true)
    })
  ]
}

module.exports = merge(base, configProd)
