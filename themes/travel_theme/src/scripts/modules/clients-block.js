export default function ($, Drupal) {
  const drupal = Drupal
  drupal.behaviors.mytheme = {
    attach(context) {
      if (context === document) {
        const $clients = $('.clients')

        $clients.first().addClass('clients-hover')
        $clients.hover(function iconNotHover() {
          if (!($(this).hasClass('clients-hover'))) {
            $(this).addClass('clients-hover').siblings().removeClass('clients-hover')
          }
        })
      }
    }
  }
}
