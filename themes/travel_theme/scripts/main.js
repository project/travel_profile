(function ($) {
  /**
   * Add red border and placeholder on error.
   */
  Drupal.behaviors.changeBorder = {
    attach: function (context) {
      var err_name = $('#webform-client-form-2', context).find('.webform-component--name').find('input.error');
      var err_mail = $('#webform-client-form-2', context).find('.webform-component--email').find('input.error');
      var err_phone = $('#webform-client-form-2', context).find('.webform-component--phone').find('input.error');
      if (err_name) {
        $('input.error').addClass('form-error');
      }
      else if (err_mail) {
        $('input.error').addClass('form-error');
      }
      else if (err_phone) {
        $('input.error').addClass('form-error');
      }
    }
  };

  /**
   * Desktop testimonials block.
   */
  Drupal.behaviors.showClientsText = {
    attach: function (context) {
      var $clients = $('.clients');
      $clients.children('.views-field-body', context).hide();
      $clients.children('.views-row:first-child .views-field-body' , context).fadeIn(500);
      $clients.first().addClass('clients-hover');

      $clients.hover(function () {
        $clients.removeClass('clients-hover');
        if (!($(this).children('.views-field-body').css('display') == 'block')) {
          $('.clients').children('.views-field-body').fadeOut(0);
        }
        if ($clients.children('.views-field-body').is(':animated')) {
          $(this).children('.views-field-body').stop(true, false);
          $(this).children('.views-field-body').fadeIn(500).fadeTo(10, 1);
        }
        else {
          $(this).children('.views-field-body').fadeIn(500).fadeTo(10, 1);
        }
      }, function () {
        if ($('.clients').children('.views-field-body').is(':animated')) {
          $(this).children('.views-field-body').stop(true, false);
          $(this).children('.views-field-body').fadeOut(500);
        }
        else {
          $(this).children('.views-field-body').fadeOut(500);
        }
        !($(this).mouseenter());
        $(this).addClass('clients-hover');
      });
    }
  };

})(jQuery);
